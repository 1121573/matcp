package matcp.model;

public class ICProporcao extends Zc {

    private double amostraN;
    private double Propor_P;
    private double p_Zero;
    
    public static double gc;
    public static double ZCVALOR;
    public static double[] grauConfianca = {90, 95, 95.45, 96, 98, 99, 99.73};
    public static double[] zc = {1.645, 1.96, 2, 2.05, 2.33, 2.58, 3};

    public ICProporcao() {
    }

    public ICProporcao(double amostraN, double Propor_P) {
        this.amostraN = amostraN;
        this.Propor_P = Propor_P;
        this.p_Zero=this.Propor_P/this.amostraN;
        

    }

    private static int pos(double gc) {

        for (int i = 0; i < grauConfianca.length; i++) {
            double d = grauConfianca[i];
            if (d == gc) {
                return i;
            }
        }
        return -1;
    }

    public double getGc() {
        return gc;
    }

    public void setGc(double val) {
        this.gc = val;
    }

    public double getAmostraN() {
        return amostraN;
    }

    public void setAmostraN(double amostraN) {
        this.amostraN = amostraN;
    }

    public double getPropor_P() {
        return Propor_P;
    }

    public void setPropor_P(double Propor_P) {
        this.Propor_P = Propor_P;
    }

    @Override
    public double delta() {
        double factor = 1e4;
        double resultado;
        double divis = this.p_Zero*(1-this.p_Zero);
        double divid = this.amostraN;
        double multi = Math.sqrt(divis/divid);
        
        resultado = ZCVALOR *multi;
        return Math.round(resultado * factor) / factor;

    }

    @Override
    public String calculaIC() {
        double delta = delta();
        double a = this.p_Zero - delta;
        double b = this.p_Zero + delta;

        return "[ " + a + " ; " + b + " ]";
    }

    @Override
    public String calculaDiferenca(Zc diferenca) {
        if (diferenca instanceof ICProporcao) {
            double delta = deltaDif(diferenca);
            double a = this.p_Zero - ((ICProporcao) diferenca).p_Zero - delta;
            double b = this.p_Zero - ((ICProporcao) diferenca).p_Zero + delta;
            double factor = 1e4;
            a = Math.round(a * factor) / factor;
            b = Math.round(b * factor) / factor;
            return "[ " + a + " ; " + b + " ]";
        }

        return "erro";
    }

    @Override
    public double deltaDif(Zc diferenca) {
        double factor = 1e4;
        double resultado = 0;

        if (diferenca instanceof ICProporcao) {
            //((Math.pow(this.desvPadr, 2)) / (this.valoresN)) + ((Math.pow(((ICProporcao) diferenca).desvPadr, 2)) / (((ICProporcao) diferenca).valoresN))
            resultado = ZCVALOR * Math.sqrt(((this.p_Zero*(1-this.p_Zero))/(this.amostraN))+((((ICProporcao) diferenca).p_Zero*(1-((ICProporcao) diferenca).p_Zero))/(((ICProporcao) diferenca).amostraN)));
        }

        return Math.round(resultado * factor) / factor;
    }

    public static void setGrauConfianca(double gc) {
        ZCVALOR = zc[pos(gc)];
    }

}
