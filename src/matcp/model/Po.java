package matcp.model;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Po {

    private double miu;

    private int valor;

    private Calculos.Operador operador;

    public Po() {
    }

    public Po(double miu, Calculos.Operador operador, int valor) {
        this.miu = miu;
        this.operador = operador;
        this.valor = valor;

    }

    public double getMiu() {
        return miu;
    }

    public void setMiu(double val) {
        this.miu = val;
    }

    public Calculos.Operador getOperador() {
        return operador;
    }

    public void setOperador(Calculos.Operador val) {
        this.operador = val;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int val) {
        this.valor = val;
    }

    private double funcaoProbabilidade() {
        return ((Math.pow(Math.E, -this.miu)) * (Math.pow(this.miu, this.valor))) / (Calculos.factorial(this.valor).longValue());
    }

    private double funcaoDistribuicao(int val) {
        java.math.BigDecimal resultado = BigDecimal.ZERO;
        double eElevadoMenosMiu = (Math.pow(Math.E, -this.miu));
        for (int i = 0; i <= val; i++) {
            double miuElevadoAI=(Math.pow(this.miu, i));
            BigInteger factI=Calculos.factorial(i);
            BigDecimal d = new BigDecimal((eElevadoMenosMiu) * (miuElevadoAI));
           d= d.divide(new BigDecimal(factI),10, java.math.RoundingMode.HALF_UP);
            resultado = resultado.add(d);
           
        }
        return resultado.doubleValue();
    }

    public double calcula() {
        double resultado = -1;

        if (operador == Calculos.Operador.Igual) {
            resultado = funcaoProbabilidade();
        } else if (operador == Calculos.Operador.Menor) {
            resultado = funcaoDistribuicao(this.valor - 1);
        } else if (operador == Calculos.Operador.MenorOuIgual) {
            resultado = funcaoDistribuicao(this.valor);
        } else if (operador == Calculos.Operador.Maior) {
            resultado = 1 - funcaoDistribuicao(this.valor);
        } else if (operador == Calculos.Operador.MaiorOuIgual) {
            resultado = 1 - funcaoDistribuicao(this.valor - 1);
        }

        double factor = 1e4;
        return Math.round(resultado * factor) / factor;
    }

}
