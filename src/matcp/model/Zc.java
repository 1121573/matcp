package matcp.model;


public abstract class Zc {

    public Zc () {
    }
    
    public abstract double delta();
    public abstract double deltaDif(Zc diferenca);
    public abstract String calculaIC();
    public abstract String calculaDiferenca(Zc diferenca);
  

}

