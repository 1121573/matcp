package matcp.model;

public class Bi {

    private int n;
    private double p;
    private int valor;
    public Calculos.Operador operador;

    public Bi() {
    }

    public Bi(int n, double p, Calculos.Operador operador, int valor) {
        this.n = n;
        this.p = p;
        this.valor = valor;
        this.operador = operador;
    }

    public int getN() {
        return n;
    }

    public void setN(int val) {
        this.n = val;
    }

    public Calculos.Operador getOperador() {
        return operador;
    }

    public void setOperador(Calculos.Operador val) {
        this.operador = val;
    }

    public double getP() {
        return p;
    }

    public void setP(double val) {
        this.p = val;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int val) {
        this.valor = val;
    }

    private double funcaoProbabilidade() {
        return (Calculos.combinacoes(this.n, this.valor).intValue()) * (Math.pow(this.p, this.valor)) * (Math.pow((1 - this.p), (this.n - this.valor)));
    }

    private double funcaoDistribuicao(int val) {
        double resultado = 0;
        for (int i = 0; i <= val; i++) {
            resultado = resultado + (Calculos.combinacoes(this.n, i).intValue()) * (Math.pow(this.p, i)) * (Math.pow((1 - this.p), (this.n - i)));
        }
        return resultado;
    }

    public double calcula() {

        double resultado = -1;

        if (operador == Calculos.Operador.Igual) {
            resultado = funcaoProbabilidade();
        } else if (operador == Calculos.Operador.Menor) {
            resultado = funcaoDistribuicao(this.valor - 1);
        } else if (operador == Calculos.Operador.MenorOuIgual) {
            resultado = funcaoDistribuicao(this.valor);
        } else if (operador == Calculos.Operador.Maior) {
            resultado = 1 - funcaoDistribuicao(this.valor);
        } else if (operador == Calculos.Operador.MaiorOuIgual) {
            resultado = 1 - funcaoDistribuicao(this.valor - 1);
        }

        double factor = 1e4;
        return Math.round(resultado * factor) / factor;
    }

}
