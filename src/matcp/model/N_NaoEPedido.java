package matcp.model;


public class N_NaoEPedido {

    private double miu;

    private double variancia;

    private double valor;

    private Calculos.Operador operador;

    public N_NaoEPedido () {
    }

    public N_NaoEPedido (double miu, double variancia, Calculos.Operador operador, double valor) {
        this.miu=miu;
        this.variancia=variancia;
        this.operador = operador;
        this.valor=valor;
        
    }
  
    public double getMiu () {
        return miu;
    }

    public void setMiu (double val) {
        this.miu = val;
    }

    public Calculos.Operador getOperador () {
        return operador;
    }

    public void setOperador (Calculos.Operador val) {
        this.operador = val;
    }

    public double getValor () {
        return valor;
    }

    public void setValor (double val) {
        this.valor = val;
    }

    public double getVariancia () {
        return variancia;
    }

    public void setVariancia (double val) {
        this.variancia = val;
    }
    
       //função erro de Gauss
    // erf(z) = 2 / raiz(pi) * integral(exp(-t*t), t = 0..z)
    private double erf(double z) {
          
          //Com base no código e informação em
          //http://www.danielsoper.com/statcalc3/calc.aspx?id=22
          //http://introcs.cs.princeton.edu/java/21function/ErrorFunction.java.html
          //http://www.danielsoper.com/statcalc3/calc.aspx?id=55
        
        double t = 1.0 / (1.0 + 0.5 * Math.abs(z));

        // use Horner's method
        double ans = 1 - t * Math.exp( -z*z   -   1.26551223 +
                                            t * ( 1.00002368 +
                                            t * ( 0.37409196 + 
                                            t * ( 0.09678418 + 
                                            t * (-0.18628806 + 
                                            t * ( 0.27886807 + 
                                            t * (-1.13520398 + 
                                            t * ( 1.48851587 + 
                                            t * (-0.82215223 + 
                                            t * ( 0.17087277))))))))));
        if (z >= 0) return  ans;
        else        return -ans;
    }      
      
    private double Phi(double z) {
          
        double factor = 1e4;
        
        //phi(x) = 1/2[1+função erro de Gauss(z/Raiz(2))]
        double resultado = 0.5 * (1.0 + erf(z / (Math.sqrt(2.0))));
        return Math.round(resultado * factor) / factor;
      
    }
    
    private double Z(){
        double factor = 1e2;
       // double resultado = ();
       // return Math.round(resultado * factor) / factor;
        return 0.0;
    }
    
    
    public double calcula () {
      
        double resultado = -1;

        if (operador == Calculos.Operador.Menor) {
           
        } else if (operador == Calculos.Operador.MenorOuIgual) {
     
        } else if (operador == Calculos.Operador.Maior) {
           
        } else if (operador == Calculos.Operador.MaiorOuIgual) {
      
        }

        double factor = 1e4;
        return Math.round(resultado * factor) / factor;
    }


}

