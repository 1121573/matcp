package matcp.model;

import java.math.BigInteger;

public abstract class Calculos {

    public static enum Operador {

        Igual, Menor, MenorOuIgual, Maior, MaiorOuIgual
    }

    public Calculos() {
     
    }

    public static BigInteger factorial(int fac) {
        BigInteger n = BigInteger.ONE;
        for (int i = 1; i <= fac; i++) {
            n = n.multiply(BigInteger.valueOf(i));
        }
        return n;
    }

    public static BigInteger combinacoes(int n, int p) {
        BigInteger den = BigInteger.ONE;
        den = factorial(p).multiply(factorial(n - p));
        return (factorial(n).divide(den));
    }
    
 
}
