package matcp.model;

public class ICMedia extends Zc {

    private double miu;

    private double desvPadr;

    private double valoresN;

    public static double gc;
    public static double ZCVALOR;
    public static double[] grauConfianca = {90, 95, 95.45, 96, 98, 99, 99.73};
    public static double[] zc = {1.645, 1.96, 2, 2.05, 2.33, 2.58, 3};

    public ICMedia() {
    }

    public ICMedia(double miu, double desvPadr, double valoresN) {
        try {

            this.miu = miu;
            this.desvPadr = desvPadr;

            this.valoresN = valoresN;

        } catch (Exception e) {

        }
    }

    private static int pos(double gc) {

        for (int i = 0; i < grauConfianca.length; i++) {
            double d = grauConfianca[i];
            if (d == gc) {
                return i;
            }
        }
        return -1;
    }

    public double getDesvPadr() {
        return desvPadr;
    }

    public void setDesvPadr(double val) {
        this.desvPadr = val;
    }

    public double getGc() {
        return gc;
    }

    public void setGc(double val) {
        this.gc = val;
    }

    public double getMiu() {
        return miu;
    }

    public void setMiu(double val) {
        this.miu = val;
    }

    public double getValores() {
        return valoresN;
    }

    public void setValores(double val) {
        this.valoresN = val;
    }

    @Override
    public double delta() {
        double factor = 1e4;
        double resultado;
        resultado = ZCVALOR * (this.desvPadr / Math.sqrt(this.valoresN));
        return Math.round(resultado * factor) / factor;

    }

    @Override
    public String calculaIC() {
        double delta = delta();
        double a = this.miu - delta;
        double b = this.miu + delta;

        return "[ " + a + " ; " + b + " ]";
    }

    @Override
    public String calculaDiferenca(Zc diferenca) {
        if (diferenca instanceof ICMedia) {
            double delta = deltaDif(diferenca);
            double a = this.miu - ((ICMedia) diferenca).miu - delta;
            double b = this.miu - ((ICMedia) diferenca).miu + delta;
            double factor = 1e4;
            a = Math.round(a * factor) / factor;
            b = Math.round(b * factor) / factor;
            return "[ " + a + " ; " + b + " ]";
        }

        return "erro";
    }

    @Override
    public double deltaDif(Zc diferenca) {
        double factor = 1e4;
        double resultado = 0;

        if (diferenca instanceof ICMedia) {
            resultado = ZCVALOR * Math.sqrt(((Math.pow(this.desvPadr, 2)) / (this.valoresN)) + ((Math.pow(((ICMedia) diferenca).desvPadr, 2)) / (((ICMedia) diferenca).valoresN)));
        }

        return Math.round(resultado * factor) / factor;
    }

    public static void setGrauConfianca(double gc) {
        ZCVALOR = zc[pos(gc)];
    }

}
